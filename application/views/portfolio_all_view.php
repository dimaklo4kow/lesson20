<H2>Наши работы:</H2>
<div>
    <?php foreach ($works AS $work): ?>
        <ul>
            <li><?=$work['url']?></li>
            <li><?=$work['description']?></li>
            <li><?=$work['year']?></li>
            <li><a href="/portfolio/work/<?=$work['id']?>">Подробнее</a></li>
        </ul>
        <br>
    <?php endforeach; ?>
</div>